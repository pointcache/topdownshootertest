﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wind : Singleton<Wind>
{
    [SerializeField] private float SpeedModifier = 1F;
    [SerializeField] private Vector2 windDirectionChangeTimingMinMax;
    [SerializeField] private Vector2 windForceMinMax;


    [SerializeField] private ParticleSystem particleSystem;
    [SerializeField] private Vector2 windDirection;
    [SerializeField] private float windForce;

    public Vector3 WindDirection
    {
        get
        {
            return new Vector3(windDirection.x * windForce, windDirection.y * windForce, 0F);
        }
    }

    private void Start()
    {
        StartCoroutine(WindDirectionChange());
    }

    private IEnumerator WindDirectionChange()
    {
        bool firstFrame = true;
        while (true)
        {
            float time = UnityEngine.Random.Range(windDirectionChangeTimingMinMax.x, windDirectionChangeTimingMinMax.y);
            if(!firstFrame)
                yield return new WaitForSeconds(time);

            firstFrame = false;

            windForce = UnityEngine.Random.Range(windForceMinMax.x, windForceMinMax.y) * SpeedModifier;
            windDirection = UnityEngine.Random.insideUnitCircle * windForce;

            var particleForceOverTimeModule = particleSystem.forceOverLifetime;
            particleForceOverTimeModule.x = windDirection.x;
            particleForceOverTimeModule.y = windDirection.y;
            //particleSystem.SetTriggerParticles = particleForceOverTimeModule;
        }
    }
}

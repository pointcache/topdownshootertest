﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHUD : MonoBehaviour
{
    [SerializeField] private Text score;
    [SerializeField] private Transform daggersRoot;

    private Player player;
    private Dagger dagger;

    private int lastAmmo;
    private int lastScore;

    private void Awake()
    {
        player = Player.Instance;
        dagger = player.GetComponent<Dagger>();
    }

    private void Update()
    {
        if (lastAmmo != dagger.AmmoCurrent)
        {
            foreach (Transform tr in daggersRoot)
                tr.gameObject.SetActive(false);

            for (int i = 0; i < dagger.AmmoCurrent; i++)
                daggersRoot.GetChild(i).gameObject.SetActive(true);

            lastAmmo = dagger.AmmoCurrent;
        }

        if (lastScore != player.Score)
        {
            score.text = player.Score.ToString();
            lastScore = player.Score;
        }
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Target : MonoBehaviour
{
    [SerializeField] private int ScoreOnHit = 10;
    public event System.Action<Transform> OnHit;



    private void OnCollisionEnter2D(Collision2D collision)
    {
        var dagger = collision.collider.GetComponent<DaggerProjectile>();
        if (dagger != null && dagger.CurrentState != DaggerProjectile.State.bounced)
        {
            OnHit(transform.parent);
            if (Player.Instance)
                Player.Instance.Score += ScoreOnHit;


            Destroy(gameObject);
        }

    }
}

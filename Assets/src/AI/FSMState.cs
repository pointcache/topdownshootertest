﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class FSMState 
{
    public enum State
    {
        success,
        failure
    }

    public virtual void Initialize(AIContext context) { }
    public abstract State Execute(AIContext context);
    public List<FSMState> childStates => new List<FSMState>();
    public bool RunChildStateOnSuccess => true;
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAICtrl : MonoBehaviour
{
    [SerializeField] private EnemyAI enemyAI;
    [SerializeField] private AIContext context;

    public EnemyAI AI => enemyAI;
    public AIContext Context => context;

    private void Start()
    {
        enemyAI.Initialize(this);
    }

    private void Update()
    {
        context.position = transform.position;
        context.rotationZ = transform.rotation.eulerAngles.z;

        ExecuteFSM();

        transform.position = context.position;
        transform.rotation = Quaternion.Euler(new Vector3(0, 0, context.rotationZ));

    }

    private void ExecuteFSM()
    {
        for (int i = 0; i < enemyAI.states.Count; i++)
        {
            ExecuteStateRecursive(enemyAI.states[i]);
        }
    }

    private void MoveTowardsTarget()
    {

    }

    private void RotateTowardsTarget()
    {

    }

    private void ExecuteStateRecursive(FSMState state)
    {
        var result = state.Execute(context);
        switch (result)
        {
            case FSMState.State.failure:
                return;
            case FSMState.State.success:
                if(state.RunChildStateOnSuccess)
                {
                    for (int i = 0; i < state.childStates.Count; i++)
                    {
                        ExecuteStateRecursive(state.childStates[i]);
                    }
                }
                break;
        }

    }
}

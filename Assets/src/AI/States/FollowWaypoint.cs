﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowWaypoint : FSMState
{
    private WaypointPath.Waypoint currentPoint;
    private WaypointPath path;
    private float timeWaiting;
    private float waitTime = 1F;

    public override void Initialize(AIContext context)
    {
        base.Initialize(context);
        path = context.waypointPath;
        currentPoint = path.GetClosestPoint(context.position);
    }

    public override State Execute(AIContext context)
    {
        if (path == null)
        {
            return State.failure;
        }

        Vector3 direction = currentPoint.position - context.position;
        // rotate towards the point
        float angle = Vector3.SignedAngle(context.transform.up, direction, -context.transform.forward);

        // move towards the point
        // reach the point
        // pause
        // find next point

        return State.failure;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScanForTarget : FSMState
{
    private Player player;

    public override void Initialize(AIContext context)
    {
        base.Initialize(context);
        player = Player.Instance;
    }

    public override State Execute(AIContext context)
    {
        if (player == null)
        {
            context.targetTransform = null;
            return State.failure;
        }

        var ptr = player.transform;

        float dist = (context.position - ptr.position).magnitude;

        if (dist < context.LineOfSightDistance)
        {
            context.targetTransform = player.transform;
            return State.success;
        }

        context.targetTransform = null;
        return State.failure;
    }
}
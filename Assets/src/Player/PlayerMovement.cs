﻿using pointcache.Utility;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(PlayerInput))]
public class PlayerMovement : MonoBehaviour
{
    public float Speed { get; private set; }

    [SerializeField] private float speed = 2F;
    [SerializeField] private float runSpeed = 5F;
    [SerializeField] private float maxSpeed = 7F;
    [SerializeField] private float deceleration = 5F;

    private PlayerInput playerInput;
    private Rigidbody2D rb2d;

    private float mx = 0F, my = 0F;
    private Vector2 cVelocity;

    private void Awake()
    {
        playerInput = GetComponent<PlayerInput>();
        rb2d = GetComponent<Rigidbody2D>();
    }


    // Update is called once per frame
    void Update()
    {
        var input = playerInput.GetInput();


        float dt = Time.deltaTime;

        float cSpeed = input.run == KeyState.pressed ? runSpeed : speed;

        var velocity = new Vector2(input.horizontal, input.vertical).normalized;
        velocity = velocity.normalized * cSpeed;
        velocity = Vector2.Lerp(cVelocity, velocity, dt * 25F);
        cVelocity = velocity;

        rb2d.velocity = velocity;

        Speed = velocity.magnitude;
    }
}

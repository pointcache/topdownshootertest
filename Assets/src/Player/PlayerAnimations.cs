﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(PlayerInput))]
public class PlayerAnimations : MonoBehaviour
{
    [SerializeField] private Transform rigRootTransform;
    [SerializeField] private SpriteRenderer spriteRenderer;

    private Animator animator;
    private PlayerInput playerInputComp;
    private PlayerMovement playerMovement;

    private void Awake()
    {
        playerInputComp = GetComponent<PlayerInput>();
        playerMovement = GetComponent<PlayerMovement>();
        animator = GetComponentInChildren<Animator>();
    }

    void Update()
    {
        var input = playerInputComp.GetInput();

        if(input.hasMovementInput)
            rigRootTransform.rotation =
                Quaternion.Lerp(rigRootTransform.rotation,
                Quaternion.Euler(new Vector3(0, 0, Vector2.SignedAngle(Vector2.up, input.movementVector))),
                Time.deltaTime * 10F);

        animator.SetFloat("Speed", playerMovement.Speed);
    }
}

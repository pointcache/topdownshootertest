﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaypointPath : MonoBehaviour
{
    private Waypoint[] points;

    public struct Waypoint
    {
        public int index;
        public Vector3 position;

        public static Waypoint NullPoint => new Waypoint()
        {
            index = -1,
            position = new Vector3()
        };
    }

    private void Awake()
    {
        points = new Waypoint[transform.childCount];

        for (int i = 0; i < transform.childCount; i++)
        {
            points[i] = new Waypoint()
            {
                position = transform.GetChild(i).position,
                index = i
            };
        }
    }

    

    public Waypoint GetClosestPoint(Vector3 position)
    {
        Waypoint closest = new Waypoint();
        float dist = float.MaxValue;


        for (int i = 0; i < points.Length; i++)
        {
            var p = points[i];
            float sqrMag = (position - p.position).sqrMagnitude;
            if (sqrMag < dist)
            {
                dist = sqrMag;
                closest = p;
            }
        }

        return closest;
    }

    public Waypoint GetNext(Waypoint current)
    {
        if(current.index >= points.Length)
        {
            return points[0];
        }

        return points[current.index + 1];
    }


    private void OnDrawGizmos()
    {
        int count = transform.childCount;

        if (count >= 2)
        {
            for (int i = 0; i < count; i++)
            {
                var tr1 = transform.GetChild(i);
                Gizmos.DrawWireCube(tr1.position, Vector3.one * 0.10F);

                if (i == count - 1)
                    return;

                var tr2 = transform.GetChild(i + 1);

                Gizmos.color = Color.green;
                Gizmos.DrawLine(tr1.position, tr2.position);
            }
        }
    }
}
